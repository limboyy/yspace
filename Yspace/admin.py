from django.contrib import admin
from .models import *
# Register your models here.
class LoveAdmin(admin.ModelAdmin):
    # fields = ['nName']
    list_display = ['classify', 'path']

class SuggestAdmin(admin.ModelAdmin):
    # fields = ['nName']
    list_display = ['uName', 'uEmail', 'uSuggest']

admin.site.register(Love, LoveAdmin)



admin.site.register(Suggest, SuggestAdmin)