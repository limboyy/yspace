from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from .models import *
from functools import wraps


def check_login(f):
    @wraps(f)
    def inner(requeset, *args, **kwargs):
        if requeset.session.get('is_login') == "True":
            return f(requeset, *args, **kwargs)
        else:
            return redirect("/")
    return inner

@csrf_exempt
def index(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        comments = request.POST.get('comments')
        sug = Suggest(uName=name, uEmail=email, uSuggest=comments)
        sug.save()
        # res = sug.save()
        # print(res)
        return HttpResponse('提交成功')
    return render(request,'Yspace/index.html')


# @check_login
def yspace(request):
    import json
    if request.method == 'POST':
        pw = request.POST.get('pw')
        res = {'static': 'False'}
        if pw == '526319':
            res['url'] = '/yspace/'
            res['static'] = 'True'
            request.session["is_login"] = 'True'
        return HttpResponse(json.dumps(res), content_type="application/json")
    if request.session.get('is_login') == "True":
        request.session.flush()
        res = []
        # loves = Love.objects.all()
        for i in range(1, 5):
            data = Love.objects.filter(classify=i)
            res.append(data)
        print(res)
        return render(request, 'Yspace/yspace.html', {'res': res})
    else:
        return redirect("/")