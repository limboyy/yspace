import numpy as np

# 插值法

# 牛顿插值法

class NewtonianInterpolation():
    def __init__(self, x, y):
        self.x = x
        self.y = y
    #
    # def quotient(self):
    #     polationList = np.zeros([1, k + 1])
    #     for i in range(x.shape[0] - 1):
    #         if i >= k:
    #             polationData = float("%.7f" % ((y[i + 1] - y[i]) / (x[i + 1] - x[i - k])))
    #             polationList = np.append(polationList, polationData)
    #     # dataList = np.append(dataList, [polationList], axis=0)
    #     if k < x.shape[0] - 1:
    #         k = k + 1
    #         data[k] = polationList
    #         interpolation(x, polationList, data, k)
    #     return data

def interpolation(x, y, k=0):
    '''
    计算差商
    '''
    data = np.zeros([x.shape[0], x.shape[0]])
    data[0] = y
    polationList = np.zeros([1, k+1])
    # print(data)
    for i in range(x.shape[0]-1):
        if i >= k:
            polationData = float("%.7f" % ((y[i+1] - y[i]) / (x[i+1] - x[i-k])))
            polationList = np.append(polationList, polationData)
    # dataList = np.append(dataList, [polationList], axis=0)
    if k < x.shape[0]-1:
        k = k + 1
        data[k] = polationList
        interpolation(x, polationList, k)
    return data


def Dpolynomail(x, y, num):
    Ddata = 0
    for i in range(y.shape[0]):
        sum = 1
        if i > 0:
            for k in range(i):
                v_sum = num - x[k]
                sum *= v_sum
            a_sum = sum * y[i][i]
        else:
            a_sum = y[i][i]
        Ddata += a_sum
    return Ddata


if __name__ == '__main__':

    x = np.array([10, 11, 12, 13, 14])
    y = np.array([2.3026, 2.3979, 2.4849, 2.5649, 2.6391])
    num = 9
    data = interpolation(x, y)
    print(Dpolynomail(x, data, num))
