from django.db import models

# Create your models here.


# Create your models here.
class Numerical(models.Model):
    '''
    1.算法名称
    2.算法介绍
    3.算法简介
    4.算法图片路径
    5.用户传入数据形式
    '''
    nName = models.CharField(max_length=20)
    nSynopsis = models.CharField(max_length=200)
    nIntroduce = models.TextField(max_length=2000)
    imgPath = models.CharField(max_length=200)
    dataForm = models.CharField(max_length=200)