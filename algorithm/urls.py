from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path(r'numerical/', views.numerical, name='numerical'),
    path(r'numNext/<id>/', views.numNext, name='numNext'),
    path(r'algAction/', views.algAction, name='algAction'),
]