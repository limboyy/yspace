from django.shortcuts import render, HttpResponse
from .models import *
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def numerical(request):
    numericals = Numerical.objects.all()
    return render(request,'algorithm/numerical.html', {'numericals': numericals})


def numNext(request, id):
    res = Numerical.objects.filter(id=id)[0]
    return render(request, 'algorithm/numerical_next.html', {'res': res})

@csrf_exempt
def algAction(request):
    if request.method == 'POST':
        parameter= request.POST.get('parameter')
        print(parameter)
        return HttpResponse('算法还在优化，不着急')