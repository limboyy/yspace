from django.contrib import admin
from .models import *
# Register your models here.
class NumericalAdmin(admin.ModelAdmin):
    # fields = ['nName']
    list_display = ['nName', 'nSynopsis']
admin.site.register(Numerical, NumericalAdmin)
